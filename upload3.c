/******************************************
Copyright 2021 Gregg Ink
Licensed under the zlib license
Compiling instructions
>> gcc -c sha256.c
this will create sha256.o
>> gcc -o upload upload3.c sha256.o

Note:
before compiling, ensure you have changed the salt
on line 139
Also ensure you have compiled and run
generate_pass_file.c with the same salt and a good password

*******************************************/

#include "stdio.h"
#include "string.h"
#include "web.h"

#define DEFAULT_UPLOAD_PATH "/var/www/html/greggink.com/public_html/uploads/"
#define PASSWORD_FILE "pass46484312.txt"

int main(){

	web_print_header();
	printf("<!DOCTYPE html><html><head><title>Upload file</title></head>");
	printf("<body bgcolor=\"#800000\"><div style=\"background-color:white;color:#00008e; padding:30px;border-radius:15px; font-size:20px; width:90%%; margin:auto;margin-top:25px;margin-bottom:25px; min-height:525px\">");

///////////////////////////////checking input

	int len = 0;
	int num = 0;
	char **fields = NULL;
	char *env = getenv("CONTENT_TYPE");
	int boundary_len = 0;

	if( tec_buf_begins(env, "multipart/form-data; boundary") ){

		char *buffer;
		char *original_buffer;
		char *boundary = env + 30;
		boundary_len = tec_string_length(boundary);
		char *cont_len = getenv("CONTENT_LENGTH");
		int original_len = 0;

			if(cont_len){
				len = tec_string_to_int(cont_len);
			original_len = len;
			len += 1;

			buffer = (char *) malloc(sizeof(char) * len );
			fread(buffer, sizeof(char), len, stdin);
		}
		original_buffer = buffer;

		//first counting how many boundaries
		int next = tec_buf_find_str(buffer, len, boundary);

		while(next != -1){

			buffer += boundary_len + next;
			len -= (boundary_len + next);
			num += 1;
			next = tec_buf_find_str(buffer, len, boundary);
		}

		num *= 4;
		num += 1;
		len = original_len;
		int index = 0;
		fields = (char **) malloc(sizeof(char *) * num );
		memset(fields, 0, sizeof(char *) * num  );
		buffer = original_buffer;
		next = tec_buf_find_str(buffer, len, boundary);
		while(next != -1){
			buffer += next - 4;
			len -= (next - 4);
			*buffer = 0;
			buffer += boundary_len + 4;
			len -= (boundary_len + 4);
			if(tec_buf_begins(buffer, "\r\nContent-Disposition: form-data;") ){
				int n = tec_string_find_char(buffer, '\"');
				buffer += n + 1;
				len -= (n + 1);
				fields[index] = buffer;
				index += 1;
				n = tec_string_find_char(buffer, '\"');
				buffer += n;
				len -= n;
				*buffer = 0;
				buffer += 1;
				len -= 1;
				if(*buffer == ';'){
					n = tec_string_find_char(buffer, '\"');
					buffer += n + 1;
					len -= (n + 1);
					fields[index] = buffer;
					index += 1;
					n = tec_string_find_char(buffer, '\"');
					buffer += n;
					len -= n;
					*buffer = 0;
					buffer += 1;
					len -= 1;
					n = tec_string_find_str(buffer, "\r\n\r\n");
					buffer += n + 4;
					len -= (n + 4);
					fields[index] = fields[index - 2];
					index += 1;
					fields[index] = buffer;

				}else{
					buffer += 4;
					len -= 4;
					fields[index] = buffer;
				}
				index += 1;
			}
			next = tec_buf_find_str(buffer, len, boundary);
		}

	}else{
//		printf("No uploads detected\n");
	}

	if(fields){

		if(tec_string_length(fields[7]) > 900){
			printf("Ooopsie<p>");
			exit(0);
		}

		char pass[1024];
		char hash[32];
		char *real = tec_file_get_contents(PASSWORD_FILE);

		tec_string_copy(pass, fields[7], 1024);
		tec_string_cat(pass, "FHerbert48976318131", 1024);
		tec_string_sha256(pass, hash);
		if(!tec_buf_cmp(hash, real, 0, 31)){
			printf("Ooopsie<p>");
			exit(0);
		}

		char path[MAX_PATH_INC];
		if(fields[5]){
			tec_string_copy(path, fields[5], MAX_PATH_INC);
			if(fields[5][tec_string_length(fields[5] - 1)] != '/'){
				tec_string_cat(path, "/", MAX_PATH_INC);
			}
		}else{
			tec_string_copy(path, DEFAULT_UPLOAD_PATH, MAX_PATH_INC);
		}

		tec_string_cat(path, fields[1], MAX_PATH_INC);
		FILE *fp = fopen(path, "wb");
		if(fp){
			fwrite(fields[3], sizeof(char), fields[4] - fields[3] - 44 - boundary_len, fp);
			fclose(fp);
			printf("successful upload<p>");
		}else{
			printf("<b>not saving</b><p>");
		}

	}

	//print html code
	printf("<form action=\"https://greggink.com/cgi-bin/file_upload/upload\" method=\"post\" enctype=\"multipart/form-data\"><input type=\"file\" name=\"the_file\"><p>");

	if(fields){
		printf("target dir <input type=\"text\" name=\"dir\" value=\"%s\" size=\"50\"><p>", fields[5]);
	}else{
		printf("target dir <input type=\"text\" name=\"dir\" value=\"%s\" size=\"50\"><p>", DEFAULT_UPLOAD_PATH);
	}
	if(fields){
		printf("<input type=\"hidden\" name=\"p\" value=\"%s\">", fields[7]);
	}else{
		printf("password <input type=\"password\" name=\"p\"><p>");
	}
	printf("<input type=\"submit\" value=\"Upload file\"></form></body></html>");

	printf("</div><div style=\"color:white;text-align:center\">File Upload</div>");
	printf("</body></html>");

	return 0;

}//main*/

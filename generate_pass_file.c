/******************************************
Copyright 2021 Gregg Ink
Licensed under the zlib license
Compiling instructions
>> gcc -c sha256.c
this will create sha256.o
>> gcc -o generate_pass_file generate_pass_file.c sha256.o

*******************************************/

#include "stdio.h"
#include "web.h"

#define PASSWORD_FILE "pass46484312.txt"

int main(){

	char hash[32];
	char pass[1024];

	tec_string_copy(pass, "1965Dune", 1024);
	tec_string_cat(pass, "FHerbert48976318131", 1024);
	tec_string_sha256(pass, hash);

	FILE *fp = fopen(PASSWORD_FILE, "wb");
	if(!fp){
		printf("could not open file to write\n");
		exit(1);
	}

	fwrite(hash, 1, 32, fp);
	fclose(fp);

}//main*/

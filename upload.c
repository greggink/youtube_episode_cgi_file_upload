#include "stdio.h"
#include "web.h"

int main(){

	web_print_header();

	char *content_type = getenv("CONTENT_TYPE");

	printf("content type: %s<br>", content_type);

	char *cont_len = getenv("CONTENT_LENGTH");
	printf("content length %s<br>", cont_len);
	int len = 0;
	char *buffer = NULL;

	if(cont_len){
		len = tec_string_to_int(cont_len);
		len += 1;
		buffer = (char *) malloc(sizeof(char) * len );
		fread(buffer, sizeof(char), len, stdin);
		buffer[len] = 0;
	}

	printf("<p><b>%s</b>", buffer);
	return 0;


}//main*/
